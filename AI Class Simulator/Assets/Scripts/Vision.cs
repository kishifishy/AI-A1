﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour {

    public GameObject owner;
    public string targetTag;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == targetTag)
        {
            owner.SendMessage("OnVisionEnter", other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == targetTag)
        {
            owner.SendMessage("OnVisionExit", other);
        }
    }
}
