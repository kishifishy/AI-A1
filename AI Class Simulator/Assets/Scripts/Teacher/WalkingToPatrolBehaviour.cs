﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WalkingToPatrolBehaviour : StateBehaviour
{
    public int waypointIndex;
    FloatVar speedVar;

    public List<Transform> waypoints;

    private void Awake()
    {
        speedVar = blackboard.GetFloatVar("speed");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started WalkingToPodium");
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped WalkingToPodium");

        waypointIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        MoveTowardWaypoint();
    }

    void MoveTowardWaypoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speedVar.Value * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == waypoints[waypointIndex].gameObject)
        {
            // check if last waypoint (exiting)
            if (waypointIndex >= waypoints.Count - 1)
            {
                SendEvent("ArriveAtPatrolRoute");
            }
            else
                UpdateWaypoint();
        }
    }

    void UpdateWaypoint()
    {
        waypointIndex = (waypointIndex + 1) % waypoints.Count;
    }
}


