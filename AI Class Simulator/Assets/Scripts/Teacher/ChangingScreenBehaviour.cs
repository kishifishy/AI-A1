﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class ChangingScreenBehaviour : StateBehaviour
{
    [SerializeField]
    float duration;
    GameObjectVar projectorScreenVar;

    //JUMPING
    [SerializeField]
    float maxJumpHeight, jumpSpeed, fallSpeed;
    float groundHeight, variatingJumpSpeed;
    Vector3 groundPos;
    public bool inputJump = false;
    public bool grounded = true;

    private void Awake()
    {
        projectorScreenVar = blackboard.GetGameObjectVar("projectorScreen");
    }

    private void Start()
    {
        groundPos = transform.position;
        groundHeight = transform.position.y;
        maxJumpHeight = transform.position.y + maxJumpHeight;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started ChangingScreen");

        Invoke("FinishChangingScreen", duration);
        grounded = true;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " started ChangingScreen");

        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update()
    {
        StartJumping();
    }

    void FinishChangingScreen()
    {
        projectorScreenVar.Value.GetComponent<WaitForTeacherInputBehaviour>().TeacherInputReceived();
        SendEvent("FinishChanging");
    }

    void StartJumping()
    {
        if (grounded)
        {
            groundPos = transform.position;
            inputJump = true;
            variatingJumpSpeed = jumpSpeed * Random.Range(0.5f, 2.0f); // varies jump speed every time by 2x slower/faster
            StartCoroutine("Jump");
        }
        if (transform.position == groundPos)
            grounded = true;
        else
            grounded = false;
    }

    IEnumerator Jump()
    {
        while (true)
        {
            if (transform.position.y >= maxJumpHeight)
                inputJump = false;
            if (inputJump)
                transform.Translate(Vector3.up * jumpSpeed * Time.smoothDeltaTime);
            else if (!inputJump)
            {
                transform.position = Vector3.Lerp(transform.position, groundPos, fallSpeed * Time.smoothDeltaTime);
                if (transform.position == groundPos)
                    StopAllCoroutines();
            }

            yield return new WaitForEndOfFrame();
        }
    }
}


