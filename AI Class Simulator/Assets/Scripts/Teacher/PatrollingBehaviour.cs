﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class PatrollingBehaviour : StateBehaviour
{
    public int waypointIndex;
    FloatVar speedVar;
    
    GameObjectVar spriteGameObjectVar;

    GameObjectVar computerToFixVar;
    GameObjectVar projectorScreenVar;

    public List<Transform> waypoints;

    private void Awake()
    {
        spriteGameObjectVar = blackboard.GetGameObjectVar("spriteObject");
        computerToFixVar = blackboard.GetGameObjectVar("computerToFix");
        projectorScreenVar = blackboard.GetGameObjectVar("projectorScreen");
        speedVar = blackboard.GetFloatVar("speed");

    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started Patrolling");
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped Patrolling");
    }

    // Update is called once per frame
    void Update()
    {
        MoveTowardWaypoint();
    }

    void MoveTowardWaypoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speedVar.Value * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == waypoints[waypointIndex].gameObject)
        {
            waypointIndex = (waypointIndex + 1) % waypoints.Count;
        }
        
    }

    void OnVisionEnter(Collider other)
    {
        // DETECTING STUDENT
        if (other.tag == "Student")
        {
            GameObject student = other.gameObject;

            // check if student needs help
            if (student.GetComponent<AskingForHelpBehaviour>().isActiveAndEnabled)
            {
                computerToFixVar.Value = student.GetComponent<Blackboard>().GetGameObjectVar("computer").Value;
                SendEvent("SeeStudentNeedsHelp");
            }
        }
        // DETECTING PROJECTOR SCREEN
        else if (other.tag == "Projector")
        {
            GameObject projector = other.gameObject;

            // check if screen needs changing
            if (projector.GetComponent<WaitForTeacherInputBehaviour>().isActiveAndEnabled)
            {
                SendEvent("SeeScreenNeedsChange");
            }
        }
    }

    void OnVisionExit()
    {

    }
}


