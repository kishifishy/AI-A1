﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class HelpingStudentBehaviour : StateBehaviour
{
    GameObjectVar computerToFixVar;
    NotWorkingBehaviour computerScript;

    private void Awake()
    {
        computerToFixVar = blackboard.GetGameObjectVar("computerToFix");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started Helping Student");

        /// CHANGE SPRITE
        /// 
        computerScript = computerToFixVar.Value.GetComponent<NotWorkingBehaviour>();
        computerScript.isBeingFixed = true;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped Helping Student");

    }

    // Update is called once per frame
    void Update()
    {
        CheckIfComputerIsFixed();
    }

    void CheckIfComputerIsFixed()
    {
        if (computerScript.isBeingFixed == false)
            SendEvent("FinishHelping");
    }
    
}


