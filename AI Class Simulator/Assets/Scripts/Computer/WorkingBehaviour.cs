﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class LightInfo
{
    public float original, target, duration;

    public LightInfo(float original, float target, float duration)
    {
        this.original = original;
        this.target = target;
        this.duration = duration;
    }
}

public class WorkingBehaviour : Interactable
{
    public Material workingMaterial;
    GameObject computerScreen;

    FloatVar efficiencyVar;
    GameObjectVar studentVar;
    // variation is so that the computers stop working at different times
    public float maxDepletionRate;
    public float depletionRateVariation;

    float maxIntensity = 3.4f; // based on value that's the same for all computers

    new Light light;

    // TO HANDLE LIGHT EFFECT
    public override void Interact()
    {
        light.intensity = maxIntensity;
        StartCoroutine("DimLight", new LightInfo(5.4f, 0, 0.5f));
    }

    IEnumerator DimLight(LightInfo lightInfo)
    {
        float startTime = Time.time;
        
        while (Time.time < startTime + lightInfo.duration)
        {
            light.intensity = Mathf.Lerp(lightInfo.original, lightInfo.target, (Time.time - startTime) / lightInfo.duration);
            yield return null;
        }

        yield return new WaitForSeconds(lightInfo.duration);
    }
    
    private void Awake()
    {
        light = GetComponentInChildren<Light>();

        computerScreen = gameObject.transform.Find("Screen").gameObject;

        efficiencyVar = blackboard.GetFloatVar("efficiency");
        studentVar = blackboard.GetGameObjectVar("student");

        // depletion rate is the MAX/FASTEST rate, then a random number is subtracted from it
        maxDepletionRate -= Random.Range(0, depletionRateVariation);
    }

    // Called when the state is enabled
    void OnEnable () {
        //Debug.Log(gameObject.name + " started Working");

        computerScreen.GetComponent<MeshRenderer>().material = workingMaterial;

    }
 
	// Called when the state is disabled
	void OnDisable () {
        //Debug.Log(gameObject.name + " stopped Working");
    }

    public void DepleteEfficiency()
    {
        efficiencyVar.Value -= maxDepletionRate * Time.deltaTime;

        if (efficiencyVar.Value <= 0)
        {
            efficiencyVar.Value = 0;
            studentVar.Value.GetComponent<StateMachine>().SendEvent("ComputerStopsWorking");
            SendEvent("StopWorking");
        }
    }
}


