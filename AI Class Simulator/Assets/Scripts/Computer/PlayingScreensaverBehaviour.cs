﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class PlayingScreensaverBehaviour : StateBehaviour
{
    public Material screensaverMaterial;
    GameObject computerScreen;

    private void Awake()
    {
        computerScreen = gameObject.transform.Find("Screen").gameObject;
    }

    // Called when the state is enabled
    void OnEnable () {
        //Debug.Log(gameObject.name + " started PlayingScreensaver");

        computerScreen.GetComponent<MeshRenderer>().material = screensaverMaterial;
    }
 
	// Called when the state is disabled
	void OnDisable () {
        //Debug.Log(gameObject.name + " stopped PlayingScreensaver");
    }
}


