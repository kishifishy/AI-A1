﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class NotWorkingBehaviour : StateBehaviour
{
    public Material notWorkingMaterial;
    GameObject computerScreen;

    FloatVar efficiencyVar;
    FloatVar maxEfficiencyVar;
    GameObjectVar studentVar;

    public float replenishRate;
    public bool isBeingFixed;

    private void Awake()
    {
        computerScreen = gameObject.transform.Find("Screen").gameObject;

        efficiencyVar = blackboard.GetFloatVar("efficiency");
        maxEfficiencyVar = blackboard.GetFloatVar("maxEfficiency");
        studentVar = blackboard.GetGameObjectVar("student");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started NotWorking");
        
        computerScreen.GetComponent<MeshRenderer>().material = notWorkingMaterial;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped NotWorking");

        isBeingFixed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isBeingFixed)
            ReplenishEfficiency();
    }

    void ReplenishEfficiency()
    {
        efficiencyVar.Value += replenishRate * Time.deltaTime;

        if (efficiencyVar.Value >= maxEfficiencyVar.Value)
        {
            efficiencyVar.Value = maxEfficiencyVar;
            studentVar.Value.GetComponent<StateMachine>().SendEvent("ComputerIsFixed");
            SendEvent("Fixed");
        }
    }
}


