﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class Player : MonoBehaviour {

    [SerializeField]
    float speed, colliderDetectionRadius;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Move();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            InteractWithInteractables();
        }        
    }

    void Move()
    {
        transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * speed * Time.smoothDeltaTime);
    }

    void InteractWithInteractables()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, colliderDetectionRadius);

        foreach (Collider collider in colliders)
        {
            StateMachine stateMachine = collider.gameObject.GetComponent<StateMachine>();
            if (stateMachine)
            {
                Interactable interactable = stateMachine.enabledState.GetComponent<Interactable>();
                if (interactable != null)
                {
                    interactable.Interact();
                }
            }
        }
    }
}
