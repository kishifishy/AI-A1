﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkingOnComputerBehaviour : Interactable
{
    public Sprite workingOnCompSprite;
    GameObjectVar spriteGameObjectVar;

    GameObjectVar projectorScreenVar;
    GameObjectVar computerVar;
    WorkingBehaviour computerScript;

    public BoolVar onBreakVar;
    FloatVar hungerVar;
    FloatVar maxHungerVar;
    public float hungerIncreaseRate;

    public override void Interact()
    {
        SendEvent("SeePlayer");
    }

    private void Awake()
    {
        spriteGameObjectVar = blackboard.GetGameObjectVar("spriteObject");
        spriteGameObjectVar.Value = gameObject.transform.Find("Student Sprite").gameObject;

        projectorScreenVar = blackboard.GetGameObjectVar("projectorScreen");
        computerVar = blackboard.GetGameObjectVar("computer");
        computerScript = computerVar.Value.GetComponent<WorkingBehaviour>();

        onBreakVar = blackboard.GetBoolVar("onBreak");
        hungerVar = blackboard.GetFloatVar("hunger");
        maxHungerVar = blackboard.GetFloatVar("maxHunger");
    }

    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started WorkingOnComputer");

        spriteGameObjectVar.Value.GetComponent<SpriteRenderer>().sprite = workingOnCompSprite;
        computerVar.Value.GetComponent<StateMachine>().SendEvent("StudentReturns");
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped WorkingOnComputer");
    }

    // Update is called once per frame
    void Update()
    {
        WorkOnComputer();
        GetHungry();

        if (onBreakVar.Value == true)
        {
            computerVar.Value.GetComponent<StateMachine>().SendEvent("StudentLeaves");
            SendEvent("GoOnBreak");
        }
    }

    void WorkOnComputer()
    {
        computerScript.DepleteEfficiency();
    }

    void GetHungry()
    {
        hungerVar.Value += hungerIncreaseRate * Time.deltaTime;

        if (hungerVar.Value >= maxHungerVar.Value)
        {
            hungerVar.Value = maxHungerVar.Value;
            computerVar.Value.GetComponent<StateMachine>().SendEvent("StudentLeaves");
            SendEvent("GetFood");
        }
    }
}


