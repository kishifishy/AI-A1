﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WalkingToComputerBehaviour : StateBehaviour
{
    public int waypointIndex;
    FloatVar speedVar;

    public Sprite walkingBackSprite;
    GameObjectVar spriteGameObjectVar;

    public List<Transform> waypoints;

    private void Awake()
    {
        speedVar = blackboard.GetFloatVar("speed");

        spriteGameObjectVar = blackboard.GetGameObjectVar("spriteObject");
        spriteGameObjectVar.Value = gameObject.transform.Find("Student Sprite").gameObject;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started WalkingToChatter");

        spriteGameObjectVar.Value.GetComponent<SpriteRenderer>().sprite = walkingBackSprite;

        waypointIndex = 0;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped WalkingToChatter");
    }

    // Update is called once per frame
    void Update()
    {
        MoveTowardWaypoint();
    }

    void MoveTowardWaypoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speedVar.Value * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == waypoints[waypointIndex].gameObject)
        {
            // check if last waypoint
            if (waypointIndex >= waypoints.Count - 1)
                SendEvent("ArriveAtComputer");
            else
                UpdateWaypoint();
        }
    }

    void UpdateWaypoint()
    {
        waypointIndex = (waypointIndex + 1) % waypoints.Count;
    }
}


