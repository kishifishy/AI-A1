﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class ChattingBehaviour : Interactable
{
    GameObjectVar computerVar;
    WorkingBehaviour computerScript;
    BoolVar onBreakVar;


    //JUMPING
    [SerializeField]
    float maxJumpHeight, jumpSpeed, fallSpeed;
    float groundHeight, variatingJumpSpeed;
    Vector3 groundPos;
    public bool inputJump = false;
    public bool grounded = true;

    public override void Interact()
    {
        SendEvent("SeePlayer");
    }

    private void Awake()
    {
        computerVar = blackboard.GetGameObjectVar("computer");
        computerScript = computerVar.Value.GetComponent<WorkingBehaviour>();
        onBreakVar = blackboard.GetBoolVar("onBreak");
    }

    private void Start()
    {
        groundPos = transform.position;
        groundHeight = transform.position.y;
        maxJumpHeight = transform.position.y + maxJumpHeight;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started Chatting");

        grounded = true;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped Chatting");

        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update()
    {
        StartJumping();

        // only check if break is over once in a while
        // this is mostly to avoid: change in Y position, and Getting Component every frame
        if (grounded)
            CheckIfBreakIsOver();
    }

    void CheckIfBreakIsOver()
    {
        if (onBreakVar.Value == false)
            SendEvent("EndBreak");
    }

    void StartJumping()
    {
        if (grounded)
        {
            groundPos = transform.position;
            inputJump = true;
            variatingJumpSpeed = jumpSpeed * Random.Range(0.5f, 2.0f); // varies jump speed every time by 2x slower/faster
            StartCoroutine("Jump");
        }
        if (transform.position == groundPos)
            grounded = true;
        else
            grounded = false;
    }

    IEnumerator Jump()
    {
        while (true)
        {
            if (transform.position.y >= maxJumpHeight)
                inputJump = false;
            if (inputJump)
                transform.Translate(Vector3.up * jumpSpeed * Time.smoothDeltaTime);
            else if (!inputJump)
            {
                transform.position = Vector3.Lerp(transform.position, groundPos, fallSpeed * Time.smoothDeltaTime);
                if (transform.position == groundPos)
                    StopAllCoroutines();
            }

            yield return new WaitForEndOfFrame();
        }
    }
}


