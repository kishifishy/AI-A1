﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WalkingToChatterBehaviour : StateBehaviour
{
    public int waypointIndex;
    FloatVar speedVar;

    public Sprite walkingTowardsSprite;
    GameObjectVar spriteGameObjectVar;

    public List<Transform> waypoints;
    public GameObject origin;

    private void Awake()
    {
        speedVar = blackboard.GetFloatVar("speed");

        spriteGameObjectVar = blackboard.GetGameObjectVar("spriteObject");
        spriteGameObjectVar.Value = gameObject.transform.Find("Student Sprite").gameObject;

        // make a new waypoint that is unique to the Student (this way they generate their own origin waypoints)
        origin = new GameObject(this.gameObject.name + " Chatter Origin");
        SphereCollider originCollider = origin.AddComponent<SphereCollider>();
        originCollider.isTrigger = true;
        originCollider.radius = 0.05f;
        waypoints[0] = origin.transform;

        // make it a child of Student Waypoints (for organization)
        origin.transform.parent = GameObject.Find("Student Waypoints").transform;

        // add this origin to the script that handles walking back
        WalkingToComputerBehaviour walkingBackScript = GetComponent<WalkingToComputerBehaviour>();
        walkingBackScript.waypoints[0] = origin.transform;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started WalkingToChatter");

        spriteGameObjectVar.Value.GetComponent<SpriteRenderer>().sprite = walkingTowardsSprite;

        // add the new waypoint to the list, make its position the student's current position
        waypoints[0] = origin.transform;
        waypoints[0].position = transform.position;

        // set new destination to [1]
        waypointIndex = 1;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped WalkingToChatter");
    }

    // Update is called once per frame
    void Update()
    {
        MoveTowardWaypoint();
    }

    void MoveTowardWaypoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speedVar.Value * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == waypoints[waypointIndex].gameObject)
        {
            // check if last waypoint
            if (waypointIndex >= waypoints.Count - 1)
                SendEvent("ArriveAtChatterPoint");
            else
                UpdateWaypoint();
        }
    }

    void UpdateWaypoint()
    {
        waypointIndex = (waypointIndex + 1) % waypoints.Count;
    }
}


