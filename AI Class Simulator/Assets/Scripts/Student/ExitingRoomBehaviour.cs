﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class ExitingRoomBehaviour : StateBehaviour
{
    public bool exiting = true;
    public int waypointIndex;
    FloatVar speedVar;

    public Sprite exitingSprite;
    public Sprite returningSprite;
    GameObjectVar spriteGameObjectVar;

    public float outsideRoomDelay;
    FloatVar hungerVar;
    GameObjectVar computerVar;

    public List<Transform> waypoints;
    GameObject origin;

    private void Awake()
    {
        speedVar = blackboard.GetFloatVar("speed");

        spriteGameObjectVar = blackboard.GetGameObjectVar("spriteObject");
        spriteGameObjectVar.Value = gameObject.transform.Find("Student Sprite").gameObject;

        hungerVar = blackboard.GetFloatVar("hunger");
        computerVar = blackboard.GetGameObjectVar("computer");

        // make a new waypoint that is unique to the Student (this way they generate their own origin waypoints)
        origin = new GameObject(this.gameObject.name + " Origin");
        SphereCollider originCollider = origin.AddComponent<SphereCollider>();
        originCollider.isTrigger = true;
        originCollider.radius = 0.05f;

        // make it a child of Student Waypoints (for organization)
        origin.transform.parent = GameObject.Find("Student Waypoints").transform;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started ExitingRoom");

        spriteGameObjectVar.Value.GetComponent<SpriteRenderer>().sprite = exitingSprite;

        // add the new waypoint to the list, make its position the student's current position
        waypoints[0] = origin.transform;
        waypoints[0].position = transform.position;

        // set new destination to [1]
        waypointIndex = 1;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped ExitingRoom");
    }

    // Update is called once per frame
    void Update()
    {
        MoveTowardWaypoint();
    }

    void MoveTowardWaypoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, waypoints[waypointIndex].position, speedVar.Value * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == waypoints[waypointIndex].gameObject)
        {
            //Debug.Log(gameObject.name + " is colliding into " + other.gameObject.name + ", " + waypointIndex);

            // check if last waypoint (RETURNING!)
            if (waypointIndex == 0)
            {
                exiting = true;

                // transition computer state
                //computerVar.Value.GetComponent<StateMachine>().SendEvent("StudentReturns");

                // transition student state
                SendEvent("SatisfyHunger");
            }

            else
            {
                // check if last waypoint (exiting) (wait then turn around)
                if (waypointIndex >= waypoints.Count - 1)
                {
                    StartCoroutine("ReturnDelay");
                    hungerVar.Value = 0;
                    spriteGameObjectVar.Value.GetComponent<SpriteRenderer>().sprite = returningSprite;
                }
                else
                {
                    // determine next waypoint depending on direction (exiting/returning)
                    UpdateWaypoint();
                }
            }
        }
    }

    IEnumerator ReturnDelay()
    {
        yield return new WaitForSeconds(outsideRoomDelay + Random.Range(-3,3));
        exiting = false;
        UpdateWaypoint();
    }

    void UpdateWaypoint()
    {
        int direction = exiting ? 1 : -1;
        waypointIndex = (waypointIndex + direction) % waypoints.Count;
    }
}


