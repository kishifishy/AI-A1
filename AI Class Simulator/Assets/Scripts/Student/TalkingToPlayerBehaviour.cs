﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class TalkingToPlayerBehaviour : StateBehaviour
{
    [SerializeField]
    List<Sprite> dialogueSpriteList;
    GameObject dialogueSprite;

    [SerializeField]
    float speed, duration;

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started TalkingToPlayer");

        CreateDialogueObject();
        SelectDialogueSprite();
        Invoke("DestroySprite", duration);
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped TalkingToPlayer");
    }

    // Update is called once per frame
    void Update()
    {
        dialogueSprite.transform.Translate(Vector3.up * speed * Time.deltaTime);
    }

    void CreateDialogueObject()
    {
        dialogueSprite = new GameObject(gameObject.name + " Dialogue");
        dialogueSprite.transform.parent = this.transform;
        dialogueSprite.transform.position = this.transform.position + new Vector3(0, 1.5f, 0);
        dialogueSprite.transform.rotation = Quaternion.Euler(0, -66, 0);
        dialogueSprite.AddComponent<SpriteRenderer>();
    }

    void SelectDialogueSprite()
    {
        SpriteRenderer spriteRendererComponent = dialogueSprite.GetComponent<SpriteRenderer>();
        spriteRendererComponent.sprite = dialogueSpriteList[Random.Range(0, dialogueSpriteList.Count)];
    }

    void MoveDialogueSprite()
    {
        dialogueSprite.transform.Translate(Vector3.up * speed * Time.deltaTime);
    }

    void DestroySprite()
    {
        Destroy(dialogueSprite);
        SendEvent("LosePlayer");
    }
}



