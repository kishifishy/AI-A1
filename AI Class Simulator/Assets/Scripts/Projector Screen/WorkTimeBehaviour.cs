﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WorkTimeBehaviour : StateBehaviour
{
    FloatVar workTimeDurationVar;
    float startTime;

    GameObjectVar studentsVar;
    StringVar previousStateVar;

    [SerializeField] Material material;
    MeshRenderer meshRendererComponent;

    private void Awake()
    {
        workTimeDurationVar = blackboard.GetFloatVar("workTimeDuration");
        studentsVar = blackboard.GetGameObjectVar("students");
        previousStateVar = blackboard.GetStringVar("previousState");

        meshRendererComponent = GetComponent<MeshRenderer>();
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(gameObject.name + " started WorkTime");

        ChangeStudentsBehaviour();

        meshRendererComponent.material = material;
        StartTimer();
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(gameObject.name + " stopped WorkTime");

        previousStateVar.Value = "Work";
    }

    // Update is called once per frame
    void Update()
    {
        if (TimerFinished())
            SendEvent("Finished");
    }

    void StartTimer()
    {
        startTime = Time.time;
    }

    bool TimerFinished()
    {
        float elapsedTime = Time.time - startTime;

        if (elapsedTime >= workTimeDurationVar.Value)
            return true;
        else
            return false;
    }

    void ChangeStudentsBehaviour()
    {
        Blackboard[] students = studentsVar.Value.GetComponentsInChildren<Blackboard>();

        // we'll only be channging the BOOL in case the students are still in other states
        // when they return from other states, they will immediately brought to break/work accordingly
        foreach (Blackboard student in students)
        {
            if (student.gameObject != studentsVar.Value)
            {
                //student.GetGameObjectVar("computer").Value.GetComponent<StateMachine>().SendEvent("StudentReturns");
                student.GetBoolVar("onBreak").Value = false;
            }
        }
    }
}


