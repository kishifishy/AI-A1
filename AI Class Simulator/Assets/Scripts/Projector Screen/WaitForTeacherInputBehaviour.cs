﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class WaitForTeacherInputBehaviour : StateBehaviour
{
    StringVar previousStateVar;

    private void Awake()
    {
        previousStateVar = blackboard.GetStringVar("previousState");
    }

    // Called when the state is enabled
    void OnEnable ()
    {
        //Debug.Log(gameObject.name + " started WaitForTeacherInput");
    }
 
	// Called when the state is disabled
	void OnDisable ()
    {
        //Debug.Log(gameObject.name + " stopped WaitForTeacherInput");
    }

    public void TeacherInputReceived()
    {
        if (previousStateVar.Value == "Work")
        {
            SendEvent("InputToBreakTime");
        }
        else if (previousStateVar.Value == "Break")
        {
            SendEvent("InputToWorkTime");
        }
    }
}


